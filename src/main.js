import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'


const router = new VueRouter({
	mode: 'history',
	scrollBehavior(to) {
		if (to.hash) {
			return {
				selector: to.hash,
				behavior: 'smooth',
			}
		}
	}
})

Vue.config.productionTip = false

new Vue({
	router,
    render: h => h(App),
}).$mount('#app')