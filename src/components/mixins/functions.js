export const functions = {
	
	methods: {
		isScrolledIntoView: function(elem) {
			var element = document.getElementById(elem);
			var docViewTop = document.documentElement.scrollTop;
			var docViewBottom = docViewTop + window.innerHeight;
			var elemTop = element.offsetTop + (element.height / 2);
			var elemBottom = elemTop + element.height;
	
			return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
		},
	
		getBitcoins(bitcoins) {
			if(bitcoins) {
				return bitcoins.split(",");
			} else {
				return [];
			}
		},
	
		sumOfBitcoins: function(bitcoins) {
			var sum = 0;
			if(bitcoins) {
				bitcoins.split(",").forEach(element => {
					sum += parseFloat(element);
				});
			}
			return sum;
		},
	
		bitcoinCapital: function(bitcoins, factor) {
			var capital = 0;
			capital = this.sumOfBitcoins(bitcoins) * (1 / factor);
			return capital;
		},
	
		getCapitalTendency: function(newCapital) {
			localStorage.setItem("oldCapital", localStorage.getItem("newCapital"));
			var oldCapital = localStorage.getItem("oldCapital");
			if(oldCapital) {
				var tendency = 0;
				if(oldCapital < newCapital) {
					tendency = 1;
				}
				if(oldCapital > newCapital) {
					tendency = -1;
				}
				localStorage.setItem("newCapital", newCapital);
				localStorage.setItem("tendency", parseInt(tendency));
			} else {
				localStorage.setItem("oldCapital", 0);
				localStorage.setItem("newCapital", 0);
				localStorage.setItem("tendency", 0);
			}
			return tendency;
		}
	}

}
